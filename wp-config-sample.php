<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0X[Qv,K5k~A/?>K-0V]ixmcu/X`C|p2jMw93jzk%Q<s`u~UgrhQv/]^P54,b+%L7');
define('SECURE_AUTH_KEY',  'MorEY>WYJnJ|T,0eNsD}.^S~ai!0tlrr0(gTVREn_w~C#$OZ_wvY[])8Nbrr(O__');
define('LOGGED_IN_KEY',    'Pk]MXy]7z>oo*Hp)C=fpR+wbe-_EDN+%|ieP`z4FByJexf>;e;,=&C9u?k&PCE>.');
define('NONCE_KEY',        '_B8|l24u]xp_mh,`sQj@cjcEz{ovW:+JUuayl~oP3kek}HjEU8PcT>J/#lU{FK1;');
define('AUTH_SALT',        'f]0XEb_3utbhNR7F!=-4&Db4~m).^D39s1:<b<rCMO:k%^I9<&QUz38tE<u$X?(k');
define('SECURE_AUTH_SALT', '(RrI7Nd]oMQ~Gyxecvjk6rA~sm@/;%pv^)[8xW!YipzC3AUJ<0/Z=qUcxWHm+z[y');
define('LOGGED_IN_SALT',   'W]Tw?4Qy$X&IhVztnFI_#%qASl=o%i %xWVVo}:<]cij4ujUhyM<<kHODu~jn@n=');
define('NONCE_SALT',       'DW|&8zR[7~o`?sQ[m]LOXWNe%GsSE%`H_@4J7$7N4_Qa75:]gh.l,)r=#NetOk>f');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
