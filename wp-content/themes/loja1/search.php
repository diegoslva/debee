<?php get_header(); ?>

<div class="container">
  <div class="listing-wrapper">
  <section class="listing">
    <div class="listing-title section-title" style="width: 100%">
      <span class="title">Resultado de busca por : '<?php echo $_REQUEST['s']; ?>' </span>
    </div>
  
    <?php while ( have_posts() ) : the_post(); ?>
      <?php include('includes/listagem.php'); ?>
    <?php endwhile; ?>
    
    <?php if(function_exists('wp_pagenavin')){wp_pagenavi();};  ?>       
  </section><!-- .listagem --> 
  <?php get_sidebar(); ?>
  </div>
</div>              
<?php  get_footer(); ?>