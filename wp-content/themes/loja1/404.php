<?php get_header(); ?>

<div class="container">
<?php get_sidebar(); ?>
    <div class="listing-wrapper">
        <?php   if ( have_posts() ) : the_post();    ?>
            <div class="section-title listing-title">
                <h1 class="page-title">Erro 404 - P�gina n�o encontrada</h1>
                <?php edit_post_link('editar', '<p class="editLink">', '</p>'); ?>
            </div>
            <section class="entry-content">
                Essa p�gina n�o pode ser encontrada. Use o menu para encontrar outras p�ginas do site.
            </section>
        <?php endif; ?>
    </div>
</div>
                
<?php  get_footer(); ?>