<?php 
//TRAVA PRECO ---------------------------------
$travaPreco = get_option('wpstravapreco');
if($travaPreco== 'sim' || $travaPreco == 'true' ){
    $travaPreco = true;
}else{
        $travaPreco = false;
          }
$usuarioConfirmado = false;
global $current_user;
get_currentuserinfo();
$idUser = $current_user->ID;    
$autorizacao = get_usermeta($idUser, 'wpsAutorizacao');
if( $autorizacao =='Confirmado' ){
     $usuarioConfirmado = true;
     $travaPreco = false;
};
//TRAVA PRECO ---------------------------------




$simbolo = get_current_symbol();


 $nomeMenuProdutos =  get_option('nomeMenuProdutos');  
 $idNomeListagem =  get_option('idNomeListagem'); 
 $idCatListagem  =  get_option('idCatListagem');  
 $totalPostListagem  =  get_option('totalPostListagem');  

   if($nomeMenuProdutos ==""){
       $nomeMenuProdutos = "Categorias";   
   }
   if($idNomeListagem ==""){
    $idNomeListagem  = "Recomendados";
    }
    
    
  if($idCatListagem==""){
  $idCatListagem = get_category_id("Produtos");
   }   
   
   if($totalPostListagem==""){
     $totalPostListagem= 3; 
   }

 $idsCatExclude   =  get_option('idsCatExclude');  
  

$url_sidebar_image_1 = get_option('url_sidebar_image_1');
 if($url_sidebar_image_1==""){
    $url_sidebar_image_1  = get_bloginfo('template_url')."/images/slide_00.jpg";
 };

 $url_sidebar_image_2 = get_option('url_sidebar_image_2');
 if($url_sidebar_image_2==""){
    $url_sidebar_image_2  = get_bloginfo('template_url')."/images/slide_00.jpg";
 };
 

$listagemOrder    = get_option('listagemOrder'); 
if($listagemOrder==""){$listagemOrder = "DESC"; }; 
$listagemOrderby   = get_option('listagemOrderby');
if($listagemOrderby==""){$listagemOrderby = "none"; };
                        
                        

?>
 
 <aside class="sidebar">
    <!-- <h3><?php echo $nomeMenuProdutos; ?></h3> -->
        
<!--    <div class="menuSidebar">
                 <?php
                  $reverse = $idsCatExclude[strlen($idsCatExclude)-1];
                  if($reverse ==","){
                        $strF =  trim(substr($idsCatExclude, 0, -1)); 
                        $strF =  trim(substr($idsCatExclude, 0, -1));  
                  }else{
                     $strF =$idsCatExclude;
                  };
                //  echo $strF."AAA";    
                  ?>
        <ul class="cats">     
            <?php wp_list_categories("title_li=&hide_empty=0&exclude=$strF"); ?>
        </ul>
    </div> -->
    
    <div class="destaque">    
        <?php
            $travaExibicao = false;
            $travaExibicaoOpt = get_option('wpstravaexibicao');

            global $current_user;
            get_currentuserinfo();
            $idUser = $current_user->ID;    

            $autorizacao = get_usermeta($idUser, 'wpsAutorizacao');
            $usuarioConfirmado = false;

            if( $autorizacao =='Confirmado' ){
                 $usuarioConfirmado = true;
                 $travaExibicao = false;
            };

        if(  ($travaExibicaoOpt== 'sim' || $travaExibicaoOpt== 'true') && $usuarioConfirmado !=true    ){ 


            $msg  = "Você necessita de autorização para visualizar os Produtos desta loja.";

            $mensagemAutorizacao = get_option('mensagemAutorizacao2WPSHOP');
            if ( is_user_logged_in() ) { 
                $mensagemAutorizacao = get_option('mensagemAutorizacaoWPSHOP');
            };

            if($mensagemAutorizacao!=""){
                $msg = $mensagemAutorizacao;
            };

            echo " <p>$msg</p> ";


        }else{ ?>
            
            
    
        <h3 class="sidebar-title"><?php echo $idNomeListagem; ?></h3>
            <div class="detalhes">
        
            <?php
                 query_posts(array(
                 'post_type' => array(  'produtos' ),
                 'cat' => ''.$idCatListagem,
                 'posts_per_page' =>''.$totalPostListagem ,
                 'order' => ''.$listagemOrder ,
                 'orderby'=>''.$listagemOrderby
                 ));
                 
            
            while ( have_posts() ) : the_post();  ?>
                
            <div class="detalhe">
                <div class="product-image-small">
                    <?php custom_get_image($post->ID, 250, 220, 0, true, true ); ?>
                </div>
                <div class="preco">
                    <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                    <?php 
                        $precoEspecial = custom_get_specialprice($post->ID); 
                        $preco = custom_get_price($post->ID);   
                        if($travaPreco!=true){
                            if($preco>0  ){ ?>

                            <span class="normal" <?php if($precoEspecial>0 ){ ?><?php echo 'style="vertical-align: text-bottom;text-decoration: line-through"' ?><?php }; ?> ><?php echo $simbolo; ?> <?php echo $preco; ?></span>
                            
                            <?php if($precoEspecial>0 ){ ?>  
                                <span class="precoDesconto"><?php echo $simbolo; ?> <?php echo$precoEspecial; ?></span>
                            <?php }; ?>                  

                            <?php }else{ ?>
                                <?php if(  $precoEspecial>0 ){ ?> 
                                <span class="precoDesconto"><?php echo $simbolo; ?> <?php echo $precoEspecial; ?></span>
                            <?php }; ?>   
                            <?php }; ?>
                    <?php }; //travapreco ?>
                </div><!-- .preco -->
            </div><!-- .detalhe -->
            
            
            <?php endwhile;wp_reset_query(); ?>
               
        </div>
        
     <?php }; //else trava produtos------ ?>
                     
        <div class="advertisement">
            <a href="<?php echo $link_sidebar_image_1; ?>" class="advertisement-item">
                <img class='advertisement-image'  src="<?php echo $url_sidebar_image_1; ?>" alt="<?php echo $url_sidebar_image_1; ?>">
            </a>
            <a href="<?php echo $title_sidebar_image_1; ?>">
                <img class='advertisement-image'  src="<?php echo $url_sidebar_image_2; ?>" alt="<?php echo $url_sidebar_image_2; ?>">
            </a>
        </div>                
    </div>
          
           
    
    <!--<div class="facebook">
        <div class="fb-like-box" data-href="https://www.facebook.com/pages/Cfcare-Hospitalar/283786798395806" data-width="230" data-height="375" data-show-faces="true" data-border-color="#ffffff" data-stream="false" data-header="false"></div>
    </div>-->
    
   
   
   
   <?php  //dynamic_sidebar( 'Sidebar'); ?>
    
    
   
</aside><!-- #sidebar -->