<?php get_header(); ?>
  <main class="product-content" id="product-content">
   <div class="close close-modal">X</div> 
   <script>
    jQuery(".close-modal").click(function() {
      jQuery(this).parent().removeClass('active');
    });

   </script>
   <div id="modal-payment" class="modal-payment"></div>
  <div class="container">
    <?php
          $travaExibicao = false;
          $travaExibicaoOpt = get_option('wpstravaexibicao');
    
          global $current_user;
          get_currentuserinfo();
          $idUser = $current_user->ID;    
          $usuarioConfirmado = false;

          $autorizacao = get_usermeta($idUser, 'wpsAutorizacao');

          if( $autorizacao =='Confirmado' ){
               $usuarioConfirmado = true;
               $travaExibicao = false;
          };
   
          if( ($travaExibicaoOpt== 'sim' || $travaExibicaoOpt== 'true') && $usuarioConfirmado !=true    ){ 
          
          
              $msg  = "Você necessita de autorização para visualizar os Produtos desta loja.";
              
              $mensagemAutorizacao = get_option('mensagemAutorizacao2WPSHOP');
              if ( is_user_logged_in() ) { 
                  $mensagemAutorizacao = get_option('mensagemAutorizacaoWPSHOP');
              };
              
              if($mensagemAutorizacao!=""){
                  $msg = $mensagemAutorizacao;
              };
              
              echo "<br/><br/>div class='contentPage'>    <div id='conteudo'><h1>$msg</h1></div></div><br/><br/>";
              
              
          }else{
                                                              
    ?>   
   
    <?php //include('includes/intro.php'); ?>
      
    <?php get_sidebar(); ?>        
    <article class="section-product-detail">
      <div class="product-image">
        <?php //custom_get_image($post->ID,304,304,1,true ,true ); ?>     
        <?php
          $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_id() ), 'full' ); 
          $image_url = $image_url[0];
        ?>
        <a class='image-big imageFirst cboxElement' href='<?php echo $image_url; ?>' target="_BLANK">
          <img class='image' src='<?php echo $image_url; ?>'/>
        </a>
        <?php custom_product_galeria(); ?>
      </div>
      
      <div class="product-summary">
        <div class='endereco'></div>
        <div class="product-title">
          <?php if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb('<div class="breadcrumbs">','</div>');
          } ?>
          <h1><?php the_title(); ?></h1>
            <?php edit_post_link('editar', '<p class="editLink">', '</p>'); ?>
        </div>
          
        <div class="freteInfo"></div>
          <div class="tags"><?php the_tags(); ?></div>

        <div class="product-resume">
          <p><?php the_excerpt(); ?></p>
        </div>              
        <?php custom_product_single(); ?>
        
          <div class="share">
            <span class="share-title">Compartilhar: </span>
            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" aria-label="Compartilhe no Facebook" onclick="window.open(this.href, 'facebook-share','width=580,height=296');return false;" title="Compartilhe no Facebook" class="share-list facebook" target='_blank'><i class="fa fa-facebook" aria-hidden="true"></i></a>

            <a aria-label="Compartilhe no Twitter" href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>&amp;<?php the_title(); ?>" onclick="window.open(this.href, 'twitter-share', 'width=550,height=235');return false;" title="Compartilhe no Twitter" class="share-list twitter" target='_blank'><i class="fa fa-twitter" aria-hidden="true"></i></a>

            <a aria-label="Compartilhe no Google Plus" href="https://plus.google.com/share?url=URL" onclick="window.open(this.href, 'google-plus-share', 'width=490,height=530');return false;" title="Compartilhe no Google+" class="property-share-item"><i class="plug plug-google-plus"></i></a>
            <a aria-label="Compartilhe no Google Plus" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" onclick="window.open(this.href, 'google-plus-share', 'width=490,height=530');return false;" title="Compartilhe no Google+" class="share-list googleplus"><i class="fa fa-google-plus" aria-hidden="true" target='_blank'></i></a>
            
            <a href="whatsapp://send?text=Veja esse: '<?php the_title(); ?>'. Segue o <?php the_permalink(); ?>" title="Compartilhe via WhatsApp" aria-label="Compartilhe no WhatsApp" class="share-list whatsapp" target='_blank'><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
          </div>
      </div>
        
    </article><!-- .entry-content -->
      <?php custom_product_abas(); ?>         
                  
   
   <?php custom_product_relation_single();  ?>                                            
                                            
  <?php }; //travaproduto ?>             

  </div><!-- #conteudo -->
</main>              
 

<?php  get_footer(); ?>