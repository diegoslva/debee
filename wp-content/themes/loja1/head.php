<?php 
/*    
if(!isset($_SESSION)){
    session_start();
}; 
*/  

$simbolo = get_current_symbol();
$templateUrl = verifyURL(get_bloginfo('template_url'));
$siteUrl = verifyURL(get_bloginfo('url'));  
$idPaginaCarrinho = get_idPaginaCarrinho(); 
$idPaginaCheckout = get_idPaginaCheckout();
    
?>
    
<!DOCTYPE HTML>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="metaurl" content="<?php echo $templateUrl; ?>">
    <title><?php wp_title(); ?></title>

    <!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:700' rel='stylesheet' type='text/css' /> -->
    
    <?php wp_head(); ?>

    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/flexslider.css">
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.flexslider-min.js"></script>

    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/colorbox/colorbox.css">  
    <script  type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.validate.js"></script>

    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/colorbox/jquery.colorbox-min.js"></script>
    <script  type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/custom.js"></script>

    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/dist/styles/styles.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/custom.css">

    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/lightbox.min.css">
    <script  type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/lightbox.min.js"></script>


    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/dist/styles/swiper.min.css">
    <script  type="text/javascript" src="<?php bloginfo('template_url'); ?>/dist/scripts/swiper.min.js"></script>
    

    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Ubuntu|Ubuntu+Mono" rel="stylesheet">
    <script src="https://use.fontawesome.com/ba1c1ed509.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Roboto" rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" media="screen" /> -->


    <style>
    .checkout {
        width: 100%;
    }

    .checkout .title-checkout {
        position: relative;
        background: #f4f2f0;
        text-align: right;
        padding: 15px;
        font-size: .9em;
        font-weight: bold;
        color: #f77274;
        text-transform: uppercase;
        margin: 2em 0;
    }
    .product-checkout-list {
        width: 100%;
        display: flex;
        flex-wrap: wrap;
        margin-bottom: 10px;
        border-bottom: 1px dashed #cac9c8;
        align-items: center;
        padding: 10px 0;
    }

    .product-checkout-image {
        width: 20%;
    }

    .product-checkout-information{
        width: 80%;
    }

    .product-checkout-item {
        display: block;
        margin-bottom: 10px;
    }
    
    .product-checkout-price,
    .product-checkout-btn {
        text-align: right;
        display: block;
    }

    .product-checkout-btn {
        display: block;
        height: 35px;
        border-radius: 3px 3px 4px 4px;
        background: #fff;
        border: 1px solid #e4e1df;
        border-bottom-width: 3px;
        color: #f77274;
        line-height: 35px;
        font-size: 12px;
        letter-spacing: 1px;
        font-weight: 600;
        text-transform: uppercase;
        max-width: 110px;
        margin-top: 15px;
        cursor: pointer;
        margin-left: auto;
        width: 100%;
        text-align: center;
    }


    .discount-coupon {
        width: 35%;
        padding: 15px;
        background-color: #f4f2f0;
        border: 1px dashed #DDD;
    }

    .discount-coupon-title {
        color: #f77274;
        margin-bottom: 1em;
    }

    .input-coupon {
        display: inline-block;
        width: 65%;
        padding: 0 10px;
        height: 48px;
        vertical-align: middle;
        border: 1px solid #e4e1df;
        border-radius: 3px;
        outline: 0;
        font-size: 15px;
        letter-spacing: .025em;
        color: #909090;
    }

    .btn-coupon {
        display: inline-block;
        vertical-align: middle;
        padding: 0 15px;
        height: 48px;
        border-radius: 3px 3px 4px 4px;
        background: #f67e74;
        border: 1px solid #e4e1df;
        border-bottom-width: 3px;
        color: #FFF;
        line-height: 45px;
        font-size: 16px;
        letter-spacing: .01em;
        font-weight: 600;
        text-align: center;
        max-width: 170px;
        margin: 0 auto;
        cursor: pointer;
    }


</style>

</head>