<?php get_header(); ?>

<div class="container">
  <?php //include('includes/intro.php'); ?>
    <?php   if ( have_posts() ) : the_post();    ?>
      <div class="titulo section-title" style="width: 100%">
        <div class="breadcrumbs">
          <?php if ( function_exists('yoast_breadcrumb') ) {
              yoast_breadcrumb('<ul id="breadcrumbs">','</ul>');
            }
          ?>
        </div>
        <?php edit_post_link('editar', '<p class="editLink">', '</p>'); ?>
        <h1 class="page-title"><?php the_title(); ?></h1>
      </div>
          <section class="entry-content">
            <?php the_content(); ?>
          </section>


    <?php endif; ?>


</div>

<?php  get_footer(); ?>