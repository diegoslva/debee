<footer id="footer" class="footer">
    <div class="container">
        <?php $valorFreteGratis = get_option('valorFreteGratis');  ?>   
                
        <?php if($valorFreteGratis !=""){ ?>
            <p class="free"><span>Frete Grátis</span> : Para toda loja nos pedidos acima de R$<?php echo $valorFreteGratis; ?>.</p>                
         <?php }; ?>
            
        <div class="footer-col">
            <?php        
             $idPaginaSobre   = get_option('idPaginaSobre');
             $idPaginaContato   = get_option('idPaginaContato'); 
             $idPaginaTermos   =  get_idPaginaTermos();
            ?>               
            <strong class="title">Informações</strong>             
            <ul>                    
                <li class="menu-list">
                    <a href="<?php echo get_permalink( $idPaginaSobre); ?>" class='menu-list-item'><?php echo get_the_title( $idPaginaSobre); ?></a></li>                      
                <li class="menu-list"><a href="<?php echo get_permalink( $idPaginaContato); ?>" class='menu-list-item'><?php echo get_the_title( $idPaginaContato); ?></a></li>                  
                <li class="menu-list"><a href="<?php echo get_permalink( $idPaginaTermos); ?>" class='menu-list-item'><?php echo get_the_title( $idPaginaTermos ); ?></a></li>                   
            </ul>           
        </div>                                              
            
        <div class="footer-col">                
            <strong class="title">Categorias</strong> 
            <ul>
                <?php
                    $idsCatExclude   =  intval(get_option('idsCatExclude'));    
                    wp_list_categories("title_li=&hide_empty=1&exclude=$idsCatExclude&depth=1"); 
                ?>
            </ul>           
        </div>                                              
            
        <div class="footer-col">                
            <strong class="title">Minha Conta</strong>  
            <?php
             $idPaginaPerfil = get_idPaginaPerfil(); 
             $idPaginaCarrinho = get_idPaginaCarrinho(); 
             $idPaginaCheckout = get_idPaginaCheckout();
             $idPaginaLogin = get_idPaginaLogin();
            ?>              
            <ul>                    
                <li class="menu-list"><a href="<?php echo get_permalink($idPaginaPerfil); ?>" class="menu-list-item"><?php echo get_the_title($idPaginaPerfil); ?></a></li>                  
                <li class="menu-list"><a href="<?php echo get_permalink($idPaginaCarrinho); ?>" class="menu-list-item"><?php echo get_the_title($idPaginaCarrinho); ?></a></li>                  
                <li class="menu-list"><a href="<?php echo get_permalink($idPaginaCheckout); ?>" class="menu-list-item"><?php echo get_the_title($idPaginaCheckout); ?></a></li>                  
                <li class="menu-list"><a href="<?php echo get_permalink($idPaginaLogin); ?>" class="menu-list-item"><?php echo get_the_title($idPaginaLogin); ?></a></li>                    
            </ul>           
        </div>  
        <div class="footer-col">
            <?php  if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("ultima coluna Footer")):?>
            <?php endif; ?>
        </div>                                
        <div class="footer-social-col">                  
           <?php 
              if (function_exists('showFormNewsletter') ) {  
               echo "<h5>Receba novidades</h5>";
                 showFormNewsletter(0,'','');    
             };
            ?>          
              <?php
                  $linkFacebook   =  get_option('linkFacebook');  
                  $linkTwitter    =  get_option('linkTwitter');   
                  $linkInstagram  =  get_option('linkInstagram');   
              ?>              
              <li class="facebook footer-social-list"><a href="<?php echo $linkFacebook; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <?php if($linkTwitter!= ''){ ?>                   
                <li class="twitter footer-social-list"><a href="<?php echo $linkTwitter; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>          
              <?php }; ?>       
              <li class="instagram footer-social-list"><a href="<?php echo $linkInstagram; ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>                 
        </div>                              
    <small class="copy"><span>© 2016 Todos os direitos reservados.</span></small>                   
    </div>                  
</footer> 


<?php wp_footer(); ?>

</body>
</html>

