<?php get_header(); ?>



<div id="content">

    <div class="espacamento"></div>
    
    
	<div id="page">
        
        <div class="container">
        
        	<div class="contentPage">
            <div class="bordaTop"></div>
            
        
        
        		<div id="conteudo">
                
                	
                    
                    <div class="titulo">
                    
                    
                        
                        
                        
                        <h1 class="page-title"><?php single_tag_title(); ?></h1>
                        
                        
                        
                       
                        <?php if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('<div class="breadcrumbs">','</div>');
						} ?>
                        
                    </div>
                    
                
                    
                    <div class="clear"></div>
                
                    
                    <section class="listagem">
                        
                      
          
                              <?php while ( have_posts() ) : the_post(); ?>

                              	<?php include('includes/listagem.php'); ?>

                              <?php endwhile;  ?>
                              
                    
                              
                              
                        
                        <div class="clear"></div>
                        
                          
                    </section><!-- .produtosRecentes --> 
                    
                            <?php wp_pagenavi(); ?>
                            
                            <br/><br/><br/>
                              
                            
                    
                    
                </div><!-- #conteudo -->
                
                
                <?php get_sidebar(); ?>
                
                
                <div class="clear"></div>
                
                
           

<?php  get_footer(); ?>