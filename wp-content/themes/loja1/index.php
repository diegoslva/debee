<?php  get_header(); 
    $travaExibicao = false;
    $travaExibicaoOpt = get_option('wpstravaexibicao');
    $usuarioConfirmado = false;
    
    global $current_user;
    get_currentuserinfo();
    $idUser = $current_user->ID;    

    $autorizacao = get_usermeta($idUser, 'wpsAutorizacao');

    if( $autorizacao =='Confirmado' ){
         $usuarioConfirmado = true;
         $travaExibicao = false;
    };
?>

<div class="modal"></div>
    <?php //include('includes/intro.php'); ?>    
    <?php putRevSlider("slider1") ?>
        
<section class='container container-home'>
    <main class="main-products">
    <?php

        if( ($travaExibicaoOpt== 'sim' || $travaExibicaoOpt== 'true') && $usuarioConfirmado !=true    ){
            $msg  = "Você necessita de autorização para visualizar os Produtos desta loja.";
            
            $mensagemAutorizacao = get_option('mensagemAutorizacao2WPSHOP');
            if ( is_user_logged_in() ) { 
                $mensagemAutorizacao = get_option('mensagemAutorizacaoWPSHOP');
            };
            
            if($mensagemAutorizacao!=""){
                $msg = $mensagemAutorizacao;
            };
            
            echo "<br/><br/><h1>$msg</h1><br/><br/>";
        }else{
          include('includes/produtosRecentes.php');
        };
                            
    ?>           
    <?php //include('includes/custom_listing_product.php') ?>
    </main>
    <?php  //$sidebarhome = get_theme_mod( 'sidebarhome' );  ?>
    <?php  if ( $sidebarhome!= 'full' ) : ?>
    <?php  get_sidebar();  ?> 
    <?php  endif; ?>
    <div style="width:100%">
    <?php include('offers.php') ?>
    </div>
    <div style="width: 100%; padding: 20px 0">
        <?php  if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Antes do rodape")):?>
        <?php endif; ?>
    </div>
</section>
<?php  get_footer(); ?>
