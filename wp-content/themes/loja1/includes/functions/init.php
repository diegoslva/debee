<?php     
   
require_once(ABSPATH . "wp-admin/includes/taxonomy.php");


     //FINAL ATUALIZA DADOS DA ASSINATURA DO USUARIO-------------------------------
     add_filter( 'the_post', 'countTimeTheme' );       
	 function countTimeTheme(){
	    
	     $count  = get_option('countTimeThemeV'); 
	     if($count!=""){
	         $count  = intval($count);
	     }else{ $count  = 0; };    
	     
              if($count<=2 ){
                    $count  += 1;   
                   //$excludePages               

                   $idPaginaSobre   = get_option('idPaginaSobre');
           		   $idPaginaTermos   =  get_idPaginaTermos();

           		   $idPaginaPedido   = get_idPaginaPedido();   
           		   $idPaginaLogin  =  get_idPaginaLogin(); 
           		   $idPaginaPagamento   =  get_idPaginaPagamento(); 

                   $idsPageExclude   =  "2,$idPaginaPedido,$idPaginaSobre,$idPaginaTermos,$idPaginaLogin,$idPaginaPagamento";  

                    //echo "+++$idsPageExclude";
                    add_option('idsPageExclude',$idsPageExclude,'','yes'); 
                    update_option('idsPageExclude',$idsPageExclude);
                   
                    add_option('countTimeThemeV',$count,'','yes');   
                    update_option('countTimeThemeV',$count);    
                                                            
                 
                    
                    $args = array( 'numberposts' => -1, 'post_type'=> 'produtos' );
                    $myposts = get_posts( $args );
										$countP = 0;
                    foreach( $myposts as $post ){  setup_postdata($post); ?>
                               <?php
                                 // Update post 
                                 $my_post = array();
                                 $my_post['ID'] = get_the_id();
                                 $my_post['post_content'] = 'Teste de produto. Aqui você coloca o texto completo de seu produto.';
                                 // Update the post into the database
                                 wp_update_post( $my_post );
                         
                   }; wp_reset_query(); 
                    
                   
                   $destaques = "";
										 
									 if($countP <=2){
                    $destaques = ",".intval(get_category_id(trim('Destaques')));  
									};
									$countP +=1;
									
                    $idsCatExclude   =  "1".$destaques;
                    add_option('idsCatExclude',$idsCatExclude,'','yes'); 
                    update_option('idsCatExclude',$idsCatExclude);
           };
      };
  
  


$firstTimeTheme = get_option('firstTimeTheme'); 
  
$version = '1.0'; 
 
if($firstTimeTheme!=$version ){   // if first time in theme (install default pages)   
    $count +=1;
    wp_get_current_user();
 
    $autorId  = $current_user->ID;
     
    
    //-------------------------PAGINA SOBRE--------------------------------------------------
    
                  
    $titulo = get_page_by_title('Sobre WpStore','ARRAY_A','page');
    $id = count($titulo);   
      if($id <=0){
        
             // Create post object
                 $my_post = array(
                 'post_title'    =>  "Sobre WpStore",
                 'post_content'  => 'Esta é uma loja modelo de WP STORE! Você pode baixar esta loja gratuitamente em <a href="http://wpstore.com.br/">http://wpstore.com.br/</a>',
                 'post_type'   => 'page',
                 'post_status'   => 'publish',
                 'post_author' => $autorId,
                 'post_category' => $arrayCat
                 ); 

                 // Insert the post into the database
                 $novoId = wp_insert_post( $my_post );   
                 
                 
                 //SET PAGE ABOUT /////////////////////     
                 
                                  $idPaginaSobre = $novoId; 
                                  add_option('idPaginaSobre',$idPaginaSobre ,'','yes'); 
                                  update_option('idPaginaSobre',$idPaginaSobre );

            
      };
    
    //-----------------------------PAGINA SOBRE---------------------------------------------- 
    
      
    
    
    //-------------------------PAGINA Termos-------------------------------------------------
    
                  
    $titulo = get_page_by_title('Termos de Uso','ARRAY_A','page');
    $id = count($titulo);   
      if($id <=0){
        
             // Create post object
                 $my_post = array(
                 'post_title'    =>  "Termos de Uso",
                 'post_content'  => 'É sempre bom que você descreva uma pagina com todos os termos e condições de uso e compras em sua loja. Informações sobre trocas, entrega , contatos ....',
                 'post_type'   => 'page',
                 'post_status'   => 'publish',
                 'post_author' => $autorId,
                 'post_category' => $arrayCat
                 ); 

                 // Insert the post into the database
                 $novoId = wp_insert_post( $my_post );  
                 

                      //SET PAGE TERMS///////////////////// 
                         $idPaginaTermos  = $novoId; 
                           add_option('idPaginaTermosWPSHOP',$idPaginaTermos  ,'','yes'); 
                           update_option('idPaginaTermosWPSHOP',$idPaginaTermos  );
      };
    
    //-----------------------------PAGINA Termos----------------------------------------------
    
    
    
    
    //-------------------------PAGINA Contatos-------------------------------------------------
    
                  
    $titulo = get_page_by_title('Contato','ARRAY_A','page');
    $id = count($titulo);   
      if($id <=0){
        
             // Create post object
                 $my_post = array(
                 'post_title'    =>  "Contato",
                 'post_content'  => 'Também é extremamente aconselhável que você coloque  em uma pagina todas as informações necessárias e formulário para que o cliente possa entrar em contato.',
                 'post_type'   => 'page',
                 'post_status'   => 'publish',
                 'post_author' => $autorId,
                 'post_category' => $arrayCat
                 ); 

                 // Insert the post into the database
                 $novoId = wp_insert_post( $my_post );      
                 
                 //SET PAGE CONTACT /////////////////////  

     
                     
                     $idPaginaContato = $novoId; 
                     add_option('idPaginaContato',$idPaginaContato ,'','yes'); 
                     update_option('idPaginaContato',$idPaginaContato );
                     
                          
                 
      };
    
    //-----------------------------PAGINA Contatos-----------------------------------------------
    
    
    
    $produtos = intval(get_category_id(trim('Produtos')));
    $computadores = intval(get_category_id(trim('Computadores')));
    $notebooks = intval(get_category_id(trim('Notebooks'))); 
    $acessorios = intval(get_category_id(trim('Acessórios')));
    $destaques = intval(get_category_id(trim('Destaques')));
       

     /*
     
     
     if($produtos<=0){
        $my_cat_id = wp_create_category("Produtos");
  	    $produtos =   $my_cat_id ;
     } */         
 
     if($computadores<=0){
          $my_cat_id = wp_create_category('Computadores' );
    	  //$my_cat = array('cat_name' => 'Computadores', 'category_description' => 'Computadores', 'category_nicename' => 'computadores', 'category_parent' => $produtos );
          //$my_cat_id = wp_insert_category($my_cat);
    	  $computadores =   $my_cat_id ;
     } 
         
       
       
     if($notebooks<=0){
       	    $my_cat_id = wp_create_category('Notebooks'); 
       	    //$my_cat = array('cat_name' => 'Notebooks', 'category_description' => 'Notebooks', 'category_nicename' => 'notebooks', 'category_parent' => $produtos );
            //$my_cat_id = wp_insert_category($my_cat);
            $notebooks =   $my_cat_id ;
     }
         
        
    
     if( $acessorios<=0){
        	 $my_cat_id = wp_create_category('Acessórios' );  
        	 //$my_cat = array('cat_name' => 'Acessórios', 'category_description' => 'Acessórios', 'category_nicename' => 'acessorios', 'category_parent' => $produtos );
             //$my_cat_id = wp_insert_category($my_cat);
             $acessorios =  $my_cat_id ;
      }
       
      
      if($destaques<=0){
          	 $my_cat_id = wp_create_category("Destaques");   
          	 //$my_cat = array('cat_name' => 'Destaques', 'category_description' => 'Destaques', 'category_nicename' => 'destaques', 'category_parent' => '' );
             //$my_cat_id = wp_insert_category($my_cat);
             $destaques =   $my_cat_id ;
        }   
        
  
                       
     
       $arrayNomes = array();
       $arrayNomes[]= 'Apple Mac Mini'; 
       $arrayNomes[]= 'Imac';
       $arrayNomes[]= 'Ipod nano 16gb';
       $arrayNomes[]= 'Ipod Shuffle';
       $arrayNomes[]= 'Macbook Pro 13';
       $arrayNomes[]= 'Iphone';  
       $arrayNomes[]= 'Teclado Apple';   
      
      
        $arraypreco = array();
        $arraypreco []= array('2.499,00',''); 
        $arraypreco []= array('6.800,00','6.300,00');  
        $arraypreco []= array('500,00','400,00'); 
        $arraypreco []= array('250,00','180,00');   
        $arraypreco []= array('3.900,00','3.700,00');    
        $arraypreco []= array('2.000,00','1.900,00'); 
        $arraypreco []= array('400,00','300,00'); 
        
     
		 
		  update_option('idCatSlide',$destaques);   
			update_option('totalPostSlide',3);
			update_option('slideOrderby','ID');
			update_option('listagemOrder','DESC');
  
					 
 
      $arrayCat = array();
      $arrayCat[]= array($produtos,$computadores,$destaques);
      $arrayCat[]= array($produtos,$computadores,$destaques);  
      $arrayCat[]= array($produtos,$acessorios,$destaques); 
			
      $arrayCat[]= array($produtos,$acessorios); 
      $arrayCat[]=  array($produtos,$notebooks);   
      $arrayCat[]= array($produtos,$acessorios);   
      $arrayCat[]= array($produtos,$acessorios);  
     
     
     for($z=0;$z<=count($arrayNomes);$z++){  
      
   if($arrayNomes[$z] !=""){   
    //INSERT PRODUCT -----------------------------------------------------
    $tit = $arrayNomes[$z]; 
    $numFoto= $z+1;
    $titulo = get_page_by_title($tit,'ARRAY_A','produtos');
    $id = count($titulo);   
       
    

    
    if($id <=0){
           
 
                                 
   
    // Create post object
      $my_post = array(
      'post_title'    =>  $tit,
      'post_content'  => 'Exemplo: Texto completo do produto.',
      'post_type'   => 'produtos',
      'post_status'   => 'publish',
      'post_author' => $autorId,
      'post_category' => array( intval($arrayCat[$z][0]) , intval($arrayCat[$z][1]) , intval($arrayCat[$z][2]) )
      ); 
        
    
       //echo "---".intval($arrayCat[$z][0])."---".intval($arrayCat[$z][1])."---".intval($arrayCat[$z][2])."---";
      // Insert the post into the database
      $novoId = wp_insert_post( $my_post );     
 
      $post_id = $novoId;   
      
       wp_set_post_categories( $post_id, array(intval($arrayCat[$z][1]) ,intval($arrayCat[$z][2]),intval($arrayCat[$z][0]) ) );
       wp_set_post_terms( $post_id, array(intval($arrayCat[$z][1]) , intval($arrayCat[$z][2]) , intval($arrayCat[$z][0]) ) , 'category' ,true);  
  
       //IMAGEM DESTACADA ------------------------------------------ 

       $upload_dir = wp_upload_dir();      
       
       $dirbase = $_SERVER['DOCUMENT_ROOT'];
       $dirbase = ".."; 
       
       
       $filename=  $dirbase."/wp-content/themes/loja1/images/example/foto$numFoto.jpg";
       
       $old = $filename;
       $new = $dirbase."/wp-content/uploads/foto$numFoto.jpg";
       copy($old, $new) or die("Unable to copy $old to $new.");
       
       $filename  = $new;
       
       $image_url = $filename;  
       
       $image_data = file_get_contents($image_url);
       $filename = basename($image_url);
       if(wp_mkdir_p($upload_dir['path']))
           $file = $upload_dir['path'] . '/' . $filename;
       else
           $file = $upload_dir['basedir'] . '/' . $filename;
       file_put_contents($file, $image_data);

       $wp_filetype = wp_check_filetype($filename, null ); 
       //print_r($wp_filetype );
       $attachment = array(
           'post_mime_type' => $wp_filetype['type'],
           'post_title' => sanitize_file_name($filename),
           'post_content' => '',
           'post_status' => 'inherit'
       );
       $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
       require_once(ABSPATH . 'wp-admin/includes/image.php');
       $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
       wp_update_attachment_metadata( $attach_id, $attach_data );

       set_post_thumbnail( $post_id, $attach_id );
       
      //END IMAGEM DESTACADA -------------------------------------- 
       
      
      
      //ADD SOME PRODUCT OPTIONS OF PLUGIN WP STORE ---------- 
       update_post_meta($post_id,'price',$arraypreco[$z][0] );  
       update_post_meta($post_id,'specialprice',$arraypreco[$z][1] ); 
     //END ADD SOME PRODUCT OPTIONS OF PLUGIN WP STORE ------      
                    
          
          $priceCompare =   str_replace('.','', $arraypreco[$z][1]);
          if($priceCompare==""){
          $priceCompare =   str_replace('.','', $arraypreco[$z][0]);    
          }  
           
            
         $priceCompare =   str_replace(',','', $priceCompare); 
         update_post_meta( $post_id, 'priceCompare' , $priceCompare );    
 
     
     
      if($arrayCat[$z][2] !=""){ 
           $filenameBanner =  get_bloginfo('url')."/wp-content/themes/loja1/images/example/banner$numFoto.png";   
           update_post_meta($post_id,'banner',$filenameBanner ); 
       }
       
     
 
          
    
    };          
    
    //END INSERT PRODUCTS-----------------------------------------------------   
    
    
     };   
     
     
   }; //end for     
   
   
       
   
       //Editar string botão 'Escolha o tamanho';
        $txtEscolhaTamanhoProduto= "escolha o tipo :"; 
        add_option('txtEscolhaTamanhoProdutoWPSHOP',$txtEscolhaTamanhoProduto,'','yes'); 
        update_option('txtEscolhaTamanhoProdutoWPSHOP',$txtEscolhaTamanhoProduto); 
        
        
        $cepOrigemCorreios = '24340000';
          add_option('cepOrigemCorreios',$cepOrigemCorreios,'','yes'); 
          update_option('cepOrigemCorreios',$cepOrigemCorreios);


        $tipoFrete = 'correios';
         add_option('tipoFrete',$tipoFrete,'','yes'); 
         update_option('tipoFrete',$tipoFrete); 
         
         
         $admin_email = get_option('admin_email'); 
         
         $emailAdmin = trim($admin_email); 
         add_option('emailAdminWPSHOP',$emailAdmin,'','yes'); 
         update_option('emailAdminWPSHOP',$emailAdmin);


         $smtpFrom = trim($admin_email); 
         add_option('smtpFromWPSHOP',$smtpFrom ,'','yes'); 
         update_option('smtpFromWPSHOP',$smtpFrom ); 
         
         
         
         $ativaPagseguro = 'ativaPagseguro';
         $ativaCielo = 'ativaCielo';
         $ativaDeposito = 'ativaDeposito';
         $ativaRetirada = 'ativaRetirada';
         
         add_option('ativaPagseguro',$ativaPagseguro,'','yes'); 
         update_option('ativaPagseguro',$ativaPagseguro);

         add_option('ativaCielo',$ativaCielo,'','yes'); 
         update_option('ativaCielo',$ativaCielo);

         add_option('ativaDeposito',$ativaDeposito,'','yes'); 
         update_option('ativaDeposito',$ativaDeposito);

         add_option('ativaRetirada',$ativaRetirada,'','yes'); 
         update_option('ativaRetirada',$ativaRetirada);
           
               add_option('firstTimeTheme',$version,'','yes');   
         update_option('firstTimeTheme',$version);
		 
};//  END if first time in theme (install default pages)   


$ReloadTheme  =  intval(get_option('ReloadTheme'));    
if($ReloadTheme != '2' && current_user_can('manage_options') ){
 $ReloadTheme = $ReloadTheme+1;
 update_option('ReloadTheme',$ReloadTheme);
 
 if($ReloadTheme==1){
  wp_redirect(get_bloginfo('url'));
  }else{
		  wp_redirect(get_bloginfo('url')."/wp-admin/admin.php?page=wpstore");
  };
};


?>