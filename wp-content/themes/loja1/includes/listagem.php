﻿ 
 <?php 
  $categoriaPrincipalName = "";	
  $categoriaPrincipalLink =  "";
  $categoriaPrincipalID =  "";		

  $categoriaPrincipalName2 = "";	
  $categoriaPrincipalLink2 =  "";
  $categoriaPrincipalID2 =  "";

  $categoriaPrincipalName3= "";	
  $categoriaPrincipalLink3 =  "";
  $categoriaPrincipalID3 =  "";

  if($categoriaPrincipalLink==""){
      $catID = get_query_var('cat');
      $categoriaPrincipalID =  $catID;
      $categoriaPrincipalName = get_cat_name( $catID);	
      $categoriaPrincipalLink =  get_category_link(  $catID );
  }
  
  foreach((get_the_category()) as $category) { 

 
	    if(intval($category->parent)==41){
	       $categoriaPrincipalID2 = $category->cat_ID;
   	    $categoriaPrincipalName2 = $category->cat_name;	
   	    $categoriaPrincipalLink2 =  get_category_link(  $categoriaPrincipalID2 );
   	};


    if (cat_is_ancestor_of(41,  $category->cat_ID)  ){ 
        if($categoriaPrincipalID2 =="" ){
             $categoriaPrincipalID2 = $category->cat_ID;
 	           $categoriaPrincipalName2 = $category->cat_name;	
 	           $categoriaPrincipalLink2 =  get_category_link(  $categoriaPrincipalID2 );
 	      }else{
 	           $categoriaPrincipalID3 = $category->cat_ID;
       	   $categoriaPrincipalName3 = $category->cat_name;	
       	   $categoriaPrincipalLink3 =  get_category_link(  $categoriaPrincipalID2 );    
 	      }
    }; 
  }; //final foreach categories
        
        
        
        if($categoriaPrincipalName !=""){ 
            $categoriaPrincipalName = $categoriaPrincipalName; 
            $categoriaPrincipalLink = $categoriaPrincipalLink;
            
        };
        
        
        if($categoriaPrincipalName2 !=""){ 
            $categoriaPrincipalName = $categoriaPrincipalName2; 
            $categoriaPrincipalLink = $categoriaPrincipalLink2;
            
        };
        
        
        if($categoriaPrincipalName3 !=""){ 
            $categoriaPrincipalName = $categoriaPrincipalName3; 
            $categoriaPrincipalLink = $categoriaPrincipalLink3;
            
        };

				//TRAVA PRECO ---------------------------------
				$travaPreco = get_option('wpstravapreco');
				if($travaPreco== 'sim' || $travaPreco == 'true' ){
					$travaPreco = true;
				}else{
					$travaPreco = false;
				}
				$usuarioConfirmado = false;
				global $current_user;
				get_currentuserinfo();
				$idUser = $current_user->ID;    
				$autorizacao = get_usermeta($idUser, 'wpsAutorizacao');
				if( $autorizacao =='Confirmado' ){
				 	 $usuarioConfirmado = true;
					 $travaPreco = false;
				};
				//TRAVA PRECO ---------------------------------

       ?>
           
<article class="product-listing card-product">
  <div class='product-wrapper-image'>
    <?php custom_get_image($post->ID, 250, 220, 0, true, true ); ?>
  </div>
   
   <?php 
    $preco = custom_get_specialprice($post->ID); 
    if($preco>0){ 
   ?>

  <?php }; ?>
     
  <div class="product-information">
    <h4 class="product-name">
      <a href="<?php the_permalink(); ?>" class="product-link"><?php the_title(); ?></a>
    </h4>
   
    <div class="product-price">
      <?php 
        $preco = custom_get_specialprice($post->ID); //PLUGIN SHOP FUNCTION --------------------------------------
          if($preco>0 && $travaPreco!=true){ 
      ?>
                
      <p class="price" style="text-decoration:line-through;color: #909090;display:inline-block;font-size:12px">R$ <?php echo custom_get_price($post->ID); ?></p>
       <p style="display: inline-block;"><span class="price priceDesc">R$ <?php echo $preco; ?>  </span> </p>
               
      <?php }else{ ?>
        <?php if( $travaPreco!=true){ ?>
         <p class="price">R$ <?php echo custom_get_price($post->ID); ?></p>
        <?php }; ?>
       <?php }; ?>
   
     </div><!-- .preco -->
     
  </div><!-- .desc -->         
         
 </article><!-- .produto -->
 
                