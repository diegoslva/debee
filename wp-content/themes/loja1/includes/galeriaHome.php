<?php
  
$idCatSlide =  get_option('idCatSlide');   
$totalPostSlide =  get_option('totalPostSlide');     
 
$slideOrderby=  get_option('slideOrderby'); 
$listagemOrder =  get_option('listagemOrder');    

?>

<div class="galeria">
    <div class="flexslider">
        <ul class="slides">
        	<?php   
				query_posts(array(
					'post_type' => array( 'post', 'produtos' ),
					'cat' => $idCatSlide,
					'posts_per_page' =>$totalPostSlide,
					'orderby'=> $slideOrderby,
					'order '=> $listagemOrder		
				));
					  
					  
			   while ( have_posts() ) : the_post(); 

                 $theLink = get_post_meta($post->ID,'link',true);
                 if($theLink ==""){
                   $theLink = get_permalink();   
                 };
			   ?>


			  <li>
				  
				    <a href='<?php echo $theLink; ?>'>
				   <?php 
				   	$imgbanner = get_post_meta($post->ID,'banner', true);
				   		if($imgbanner !=""){ ?>
				  		<img width="710" height="394" src="<?php echo $imgbanner; ?>" alt="">
							<?php
								}else{
				   			?>

					<?php custom_get_image($post->ID,710,394,1 ); ?>
					<?php
					 }; 
					 ?>
					 
					</a> 
				  
			  </li>
			   

		   <?php endwhile;wp_reset_query(); ?>

        </ul>
    </div>
</div><!-- .galeria -->