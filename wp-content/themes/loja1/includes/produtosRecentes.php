<?php 

$simbolo = get_current_symbol();


$idCatListagemHome  =  get_option('idCatListagemHome');  
if($idCatListagemHome==""){ $idCatListagemHome  = get_category_id("Produtos"); };   
$totalPostListagemHome  =  get_option('totalPostListagemHome');  
if($totalPostListagemHome==""){ $totalPostListagemHome= 9;   };
$listagemHomeOrder    = get_option('listagemHomeOrder');  
if($listagemHomeOrder==""){$listagemOrder = "DESC"; }; 
$listagemHomeOrderby   = get_option('listagemHomeOrderby'); 
if($listagemHomeOrderby==""){$listagemHomeOrderby = "none"; };


//TRAVA PRECO ---------------------------------
  $travaPreco = get_option('wpstravapreco');
  if($travaPreco== 'sim' || $travaPreco == 'true' ){
  	$travaPreco = true;
  }else{
  		$travaPreco = false;
  }
  $usuarioConfirmado = false;
  global $current_user;
  get_currentuserinfo();
  $idUser = $current_user->ID;    
  $autorizacao = get_usermeta($idUser, 'wpsAutorizacao');
  if( $autorizacao =='Confirmado' ){
   	 $usuarioConfirmado = true;
  	 $travaPreco = false;
  };
//TRAVA PRECO ---------------------------------
?>

<section class="section-product">
  <h3 class="section-title"><span class="title">Produtos em destaque</span></h3>

    <?php   
      wp_reset_query(); 
   
      $simbolo = get_current_symbol();    
      
      $paged = intval( get_query_var( 'paged' ) );      

      if($paged<=0){
          $paged = 1;
      }    
            
      if($currentCat>0){    
 
       query_posts(array(
          'post_type' => array(  'produtos' ),
          'cat' => ''.$currentCat,
          'posts_per_page' =>''.$totalPostListagemHome ,
          'paged' =>''.$paged,   
          'order' => ''.$listagemHomeOrder ,
          'orderby'=>''.$listagemHomeOrderby
          ));
            
      } 
      else {
        query_posts(array(
           'post_type' => array(   'produtos' ),
            'posts_per_page' =>''.$totalPostListagemHome , 
            'cat' => ''.$idCatListagemHome,   
            'paged' =>''.$paged,   
           'order' => ''.$listagemHomeOrder ,
           'orderby'=>''.$listagemHomeOrderby
           ));
      }
    
      if($listagemHomeOrder=='rand'){
				query_posts(array(
         'post_type' => array(   'produtos' ),
          'posts_per_page' =>''.$totalPostListagemHome , 
          'cat' => ''.$idCatListagemHome,   
          'paged' =>''.$paged,   
         'order' => ''.$listagemHomeOrder ,
         ));
	
      }        
      while ( have_posts() ) : the_post(); 

    ?>
                  
    <div class="card-product card-product-medium">
      <?php 
        $preco = custom_get_specialprice($post->ID); 
        if($preco>0 && $travaPreco!=true){ 
      ?>
							   							   
			<?php }; ?>
			<div class="product-wrapper-image">  
        <?php custom_get_image($post->ID, 250, 220, 0, true, true); //PLUGIN SHOP FUNCTION  ?>
      </div>
        <div class="action-product">
          <a href="<?php the_permalink(); ?>" class="btn-actions btn-watch">
            <i class="fa fa-eye" aria-hidden="true"></i>
          </a>
          <a class="btn-actions btn-purchase botao arrend" href="<?php the_permalink(); ?>"><?php echo get_txtComprarBtProduto(); ?>
            <span class="product-more"></span>
          </a>
          <a class="btn-actions btn-shopping addCarrinho btComprar" href="<?php the_permalink(); ?>"><i class="fa fa-shopping-bag" aria-hidden="true"></i></a>
        </div>

      <div class="product-information">
				<h4 class="product-name">
          <a href="<?php the_permalink(); ?>" class="product-link"><?php the_title(); ?></a>
        </h4>
        
        <div class="product-price">
          <?php 
            $precoEspecial = custom_get_specialprice($post->ID); 
            $preco = custom_get_price($post->ID);   

            if($travaPreco!=true){
              if($preco>0 ){ ?>
                <span class="price <?php if($precoEspecial>0 ){ ?><?php echo 'old-price' ?><?php }; ?>"><?php echo $simbolo; ?> <?php echo $preco;   ?></span>
             
                <?php if( $preco>0 && $precoEspecial>0 ){ ?>
                  <span class="price-porcent-reduction">-<?php echo 100 - intval($precoEspecial*100/$preco); ?>%</span>
                <?php }; ?>
            
                <?php if($precoEspecial>0 ){ ?>
                  <span class="price"><?php echo $simbolo; ?> <?php echo$precoEspecial; ?></span>
                <?php }; ?>  
        




              <?php } else { ?>
                <?php if(  $travaPreco!=true){ ?>
                  <p class="price"><?php echo $simbolo; ?> <?php echo custom_get_price($post->ID);  ?></p>
                <?php }; ?>         
              <?php }; ?>

            <?php }; //travapreco ?>  
        </div>
      </div>
    </div>

    <?php endwhile;wp_reset_query(); ?>
               
</section><!-- .produtosRecentes -->