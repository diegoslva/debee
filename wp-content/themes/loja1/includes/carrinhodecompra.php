
<div class="cart carrinho arrend hidden-mobile" data-dropdown>
    <p>
        <span>
            <span class="countbadge"><?php echo $qtdStock; ?></span><?php //if($qtdStock<=0){ echo ' itens'; }else{ echo 'produto(s)'; }; ?>
            <i class="fa fa-shopping-cart icon-header" aria-hidden="true"></i>
        </span>
    </p>

    <div class="mini-cart cartEscondido arrend">
        <div class="triangle-with-shadow"></div>
        <table id="cart">  
            <?php   
                $arrayCarrinho = "";  
                $blogid = intval(get_current_blog_id());  
            
                if( $blogid>1 ) {
                    $arrayCarrinho = $_SESSION['carrinho'.$blogid];
                }

                else { $arrayCarrinho = $_SESSION['carrinho']; };
                    
                if( $arrayCarrinho=="" ) {
                    $arrayCarrinho = array();
                }
                   
                $subtotal = 0;
                $pesoTotal = 0;
                $infoCupom =   get_session_cupom();
                $numeroCUpom = $infoCupom[0];
                $tipoDesconto = $infoCupom[1];
                $valorDesconto = $infoCupom[2];

                foreach($arrayCarrinho as $key=>$item){ 

                $postID = intval($item['idPost']);

                if( $postID>0 ) {

                    $tabelaVariacao = $item['variacaoProduto'];
                    if($tabelaVariacao==""){
                        $tabelaVariacao="-";
                    }

                   $preco =  custom_get_price( $postID );
                   $specialPrice =  custom_get_specialprice( $postID );
                   $pesoTotal += get_weight_product($postID );

                   if($specialPrice >0){
                        $preco =  $specialPrice;   
                    }

                   $precoSoma = $preco;
                    if(strlen( $precoSoma)>=6 ) {
                        $precoSoma= str_replace('.','',$precoSoma );
                    }
                    
                    $precoSoma = str_replace(',','.',$precoSoma);   
                    $precoAdd = get_price_product_variation($postID,$tabelaVariacao);

                    //$precoAdddArray = explode('(R$',$precoAdd);
                    $precoAdddArray = explode('(',$precoAdd);
                    $sinal = $precoAdddArray[0]; 
                    $precoAddF= str_replace(')','',$precoAdddArray[1]);
                                
                    if( strlen($precoSoma)>=6 ){ 
                       $precoAddFSoma =  str_replace('.','',$precoAddF);
                       $precoAddFSoma =  str_replace(',','.',$precoAddFSoma );
                   
                    }
                    else {
                        $precoAddFSoma =  str_replace(',','.',$precoAddF);
                    };  
             
                    if( $sinal=="-" ){
                        $precoSoma = $precoSoma -  $precoAddFSoma;  
                     }

                     elseif($sinal=="+"){
                        $precoSoma = $precoSoma +  $precoAddFSoma;    
                     };   

                    $qtd        = intval($item['qtdProduto']);
                    $precoLinha = getPriceFormat($qtd*$precoSoma);
                    $subtotal  += $qtd*$precoSoma;
          
            ?>

            <tr>
                <td class="produtoI"><a href="#"><?php echo get_the_title($postID); ?></a></td>
                <td class="precoI"><?php echo $qtd; ?> x <?php echo "$simbolo $preco<br/>$precoAdd"; ?></td>
                <td class="fecharI">  <span class='removeProdCart remover' ><a href='<?php echo get_permalink( $idPaginaCarrinho)."?act=remove&idp=$key"; ?>'><i class="fa fa-trash" aria-hidden="true"></i></a></span>   </td>
            </tr>
            <?php }; }; ?>
            
        </table>
                
        <div class="inputI">
            <span class="lr"><span>Total : <?php echo  $simbolo."".custom_get_total_price_session_order(); ?> </span>
            <div class="clear"></div>
        </div>                   
        <a href="<?php echo get_permalink( $idPaginaCarrinho); ?>" class="botao arrend btn-cart">Carrinho</a>
        <a href="<?php echo get_permalink( $idPaginaCheckout); ?>" class="botao arrend btn-buy">Finalizar</a>
    </div>
</div>

<div class="cart carrinho arrend hidden-desktop">
    <p data-dropdown-mobile>
        <span>
            <span class="countbadge"><?php echo $qtdStock; ?></span><?php //if($qtdStock<=0){ echo ' itens'; }else{ echo 'produto(s)'; }; ?>
            <i class="fa fa-shopping-cart icon-header" aria-hidden="true"></i>
        </span>
    </p>

    <div class="mini-cart cartEscondido arrend">
        <table id="cart">  
            <?php   
                $arrayCarrinho = "";  
                $blogid = intval(get_current_blog_id());  
            
                if( $blogid>1 ) {
                    $arrayCarrinho = $_SESSION['carrinho'.$blogid];
                }

                else { $arrayCarrinho = $_SESSION['carrinho']; };
                    
                if( $arrayCarrinho=="" ) {
                    $arrayCarrinho = array();
                }
                   
                $subtotal = 0;
                $pesoTotal = 0;
                $infoCupom =   get_session_cupom();
                $numeroCUpom = $infoCupom[0];
                $tipoDesconto = $infoCupom[1];
                $valorDesconto = $infoCupom[2];

                foreach($arrayCarrinho as $key=>$item){ 

                $postID = intval($item['idPost']);

                if( $postID>0 ) {

                    $tabelaVariacao = $item['variacaoProduto'];
                    if($tabelaVariacao==""){
                        $tabelaVariacao="-";
                    }

                   $preco =  custom_get_price( $postID );
                   $specialPrice =  custom_get_specialprice( $postID );
                   $pesoTotal += get_weight_product($postID );

                   if($specialPrice >0){
                        $preco =  $specialPrice;   
                    }

                   $precoSoma = $preco;
                    if(strlen( $precoSoma)>=6 ) {
                        $precoSoma= str_replace('.','',$precoSoma );
                    }
                    
                    $precoSoma = str_replace(',','.',$precoSoma);   
                    $precoAdd = get_price_product_variation($postID,$tabelaVariacao);

                    //$precoAdddArray = explode('(R$',$precoAdd);
                    $precoAdddArray = explode('(',$precoAdd);
                    $sinal = $precoAdddArray[0]; 
                    $precoAddF= str_replace(')','',$precoAdddArray[1]);
                                
                    if( strlen($precoSoma)>=6 ){ 
                       $precoAddFSoma =  str_replace('.','',$precoAddF);
                       $precoAddFSoma =  str_replace(',','.',$precoAddFSoma );
                   
                    }
                    else {
                        $precoAddFSoma =  str_replace(',','.',$precoAddF);
                    };  
             
                    if( $sinal=="-" ){
                        $precoSoma = $precoSoma -  $precoAddFSoma;  
                     }

                     elseif($sinal=="+"){
                        $precoSoma = $precoSoma +  $precoAddFSoma;    
                     };   

                    $qtd        = intval($item['qtdProduto']);
                    $precoLinha = getPriceFormat($qtd*$precoSoma);
                    $subtotal  += $qtd*$precoSoma;
          
            ?>

            <tr>
                <td class="itens"><?php echo $qtd; ?></td>
                <td class="produtoI"><a href="#"><?php echo get_the_title($postID); ?></a></td>
                <td class="precoI"><?php echo "$simbolo $preco<br/>$precoAdd"; ?></td>
                <td class="fecharI">  <span class='removeProdCart remover' ><a href='<?php echo get_permalink( $idPaginaCarrinho)."?act=remove&idp=$key"; ?>'>X</a></span>   </td>
            </tr>
            <?php }; }; ?>
            
        </table>
                
        <div class="inputI">
            <span class="lr"><span>Total : <?php echo  $simbolo."".custom_get_total_price_session_order(); ?> </span>
            <div class="clear"></div>
        </div>                   
        <a href="<?php echo get_permalink( $idPaginaCarrinho); ?>" class="botao arrend btn-cart">Carrinho</a>
        <a href="<?php echo get_permalink( $idPaginaCheckout); ?>" class="botao arrend btn-buy">Finalizar</a>
    </div>
</div>