

<div class="marcas">

	<div class="container">
    
        <div class="contMarcas">
            <h5 class="hide-phone"><img src="<?php bloginfo('template_url'); ?>/images/subtituloMarcas.png" width="15" height="239" alt="Marcas" /></h5>
            <h6 class="subtitulo2 show-phone hide-desktop">Marcas em destaque</h6>
            
            
            <div class="flex-container">
                
                <div class="slider-top"></div>
                <div class="flexslider">
                    <ul class="slides">
        
                    <?php 
                           query_posts(array(
                                        'post_type' => array( 'slideInferior' ),
                                         'posts_per_page' =>4 ));

                          while ( have_posts() ) : the_post(); 

                          ?>

                          <li><a href="<?php  echo trim(get_post_meta($post->ID,'link',true)); ?>">	   	<?php custom_get_image($post->ID,970,238,0 ); ?> </a> </li>

                          <?php endwhile;wp_reset_query(); ?>
                    
                    
                    
                    </ul>
                </div>
                <div class="slider-bottom"></div>
            </div>
    
    
    
        </div><!-- .contMarcas -->
    </div><!-- .container -->
    
    <script type="text/javascript">
        $(window).load(function() {
            $('.flexslider').flexslider();
        });
    </script>
</div><!-- .marcas -->