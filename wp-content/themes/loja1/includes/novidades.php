<section class="novidades">
    <h3 class="subtitulo2">Novidades</h3>
    



<?php 

    query_posts(array(
         'post_type' => array( 'post'  ),
          'posts_per_page' =>5,
            'cat'=>226 )); 

    while ( have_posts() ) : the_post(); 

    ?>


    <div class="novidade">
        <small><?php the_date(); ?></small>
        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
        <p><?php the_excerpt(); ?></p>
        <span class="detalhes"><a href="<?php the_permalink(); ?>">Detalhes</a></span>
    </div><!-- .novidade -->
    
 


    <?php endwhile;wp_reset_query(); ?>



    
    
</section>