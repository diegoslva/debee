<?php 
  $cat_args = array(
    'post_type' => array( 'post', 'produtos' ),
    'orderby' => 'name',
    'order' => 'ASC'
  );
 
  $categories = get_categories($cat_args);
 
  foreach($categories as $category) {
      $args = array(
        'showposts' => -1,
        'category__in' => array($category->term_id),
        'caller_get_posts'=> 1
      );
 
      $posts=get_posts($args);
 
      if ($posts) {
          echo '<p>Categoria: <a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> </p> ';
 
          echo '<ul>';
          foreach($posts as $post) {
            setup_postdata($post);
 
            echo '<li><a href="' . get_permalink() .'" rel="bookmark" title="Permanent Link to '. the_title_attribute('echo=0') . '">' . get_the_title() . ' </a></li>';
 
          } // foreach($posts
          echo '</ul>';
 
        } // if ($posts
 
  } // foreach($categories
?>


<section class="section-product offers">
    <h3 class="section-title title-2">
      <strong class="title title-2"><span class="two-color">Ofertas</span> | Especiais</strong>

<!--         <div>
          <div class="swiper-button-next"></div>
          <div class="swiper-button-prev"></div>
        </div> -->
    </h3>

    <div class="swiper-container">
      <div class="swiper-wrapper">
        <?php 
          query_posts(array(
            'post_type' => array( 'post', 'produtos' ),
            'cat' => 1,
            'posts_per_page' => 20 ));

           while ( have_posts() ) : the_post(); 
        ?>
          <article class="swiper-slide card-product card-product-small">
            <?php custom_get_image($post->ID, 218, 218 ); ?>
               <div class="product-information">
                   <h3 class="product-name"><a href="<?php the_permalink(); ?>" class="product-link"><?php the_title(); ?></a></h3>

                    <div class="product-price">        
                      <?php  
                        if($preco>0  && $travaPreco!=true ){ 
                      ?>
                      <p class="old-price" style="text-decoration:line-through">
                        <?php echo $simbolo; ?> <?php echo custom_get_price($post->ID); ?>
                      </p>

                      <span class="price"><?php echo $simbolo; ?> <?php echo $preco; ?></span>

                      <?php }else{ ?>
                      
                        <?php if(  $travaPreco!=true){ ?>
                          <p class="price"><?php echo $simbolo; ?> <?php echo custom_get_price($post->ID);  ?></p>
                        <?php }; ?>
                      <?php }; ?>
                    </div>
               </div>
              <div class="action-product">
                <a href="<?php the_permalink(); ?>" class="btn-watch">
                  <i class="fa fa-eye" aria-hidden="true"></i>
                </a>
                <a class="btn-purchase botao arrend" href="<?php the_permalink(); ?>"><?php echo get_txtComprarBtProduto(); ?>
                  <span class="product-more"></span>
                </a>
                <a class="btn-shopping addCarrinho btComprar" href="<?php the_permalink(); ?>"><i class="fa fa-shopping-bag" aria-hidden="true"></i></a>
              </div>
          </article>
      <?php endwhile;wp_reset_query(); ?>
      </div>
    </div>
  
</section>