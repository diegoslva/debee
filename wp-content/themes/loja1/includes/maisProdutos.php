<section class="section-product offers">
    <h3 class="section-title title-2">
      <strong class="title title-2"><span class="two-color">Ofertas</span>  | Especiais</strong>
<!--         <div>
          <div class="swiper-button-next"></div>
          <div class="swiper-button-prev"></div>
        </div> -->
    </h3>

    <div class="swiper-container">
      <div class="swiper-wrapper">
        <?php 
          query_posts(array(
            'post_type' => array( 'post', 'produtos' ),
            'cat' => 1,
            'posts_per_page' => 20 ));

           while ( have_posts() ) : the_post(); 
        ?>
          <article class="swiper-slide card-product card-product-small">
            <span class="flag">Oferta especial</span>
            <div class="product-wrapper-image">
              <?php custom_get_image($post->ID, 250, 220, 0, true, true ); ?>
            </div>
               <div class="product-information">
                   <h3 class="product-name"><a href="<?php the_permalink(); ?>" class="product-link"><?php the_title(); ?></a></h3>
                    <div class="product-price">      
          <?php 
            $precoEspecial = custom_get_specialprice($post->ID); 
            $preco = custom_get_price($post->ID);   

            if($travaPreco!=true){
              if($preco>0 ){ ?>
                <span class="price <?php if($precoEspecial>0 ){ ?><?php echo 'old-price' ?><?php }; ?>"><?php echo $simbolo; ?> <?php echo $preco;   ?></span>
             
                <?php if( $preco>0 && $precoEspecial>0 ){ ?>
                  <span class="price-porcent-reduction">-<?php echo 100 - intval($precoEspecial*100/$preco); ?>%</span>
                <?php }; ?>
            
                <?php if($precoEspecial>0 ){ ?>
                  <span class="price"><?php echo $simbolo; ?> <?php echo$precoEspecial; ?></span>
                <?php }; ?>  
        




              <?php } else { ?>
                <?php if(  $travaPreco!=true){ ?>
                  <p class="price"><?php echo $simbolo; ?> <?php echo custom_get_price($post->ID);  ?></p>
                <?php }; ?>         
              <?php }; ?>

            <?php }; //travapreco ?> 
                    </div>
               </div>
              <div class="action-product">
                <a href="<?php the_permalink(); ?>" class="btn-actions btn-watch">
                  <i class="fa fa-eye" aria-hidden="true"></i>
                </a>
                <a class="btn-actions btn-purchase botao arrend" href="<?php the_permalink(); ?>"><?php echo get_txtComprarBtProduto(); ?>
                  <span class="product-more"></span>
                </a>
                <a class="btn-actions btn-shopping addCarrinho btComprar" href="<?php the_permalink(); ?>"><i class="fa fa-shopping-bag" aria-hidden="true"></i></a>
              </div>
          </article>
      <?php endwhile;wp_reset_query(); ?>
      </div>
    </div>
  
</section>