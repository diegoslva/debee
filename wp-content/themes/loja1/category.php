﻿<?php get_header(); 

       $simbolo = get_current_symbol();

        $currentCat = intval(get_query_var('cat'));  

       $totalPostCat =  number_postpercat($currentCat); 
       
       $descricao = category_description( $currentCat );
 
    
?>

<div class="container">
  <div class="listing-wrapper">
    <?php get_sidebar(); ?>
                
    <div class="listing">
      <div class='wrapper-order'>
      <div class="listing-title section-title">
        <span class="title">Foram encontrados <?php echo $totalPostCat ; ?> <?php if($totalPostCat >1){ echo "produtos"; }else{ echo "produto"; }; ?></span>
      </div>
      <div class="ordenar">
        <form>   
          <input type="hidden" name='idCat' id="idCat" value="<?php echo $currentCat; ?>" />
              <label>Ordenar por</label>
              <select id='filtroBuscaProduto'>
                  <option value='todos'>--</option>
                  <option value='precoAsc'> Preço Crescente </option>
                  <option value='precoDesc'> Preço Decrescente </option>
                  <option value='titleAsc'> Nome do produto: A-Z </option>
                  <option value='titleDesc'> Nome do produto: Z-A </option>
              </select> 
        </form>
      </div>
      </div>
        <?php
          $catID = get_query_var('cat'); 
          $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
          $catOrder = get_option('catOrder');  
          $catOrderby = get_option('catOrderby'); 
          if($catOrder!=''){
            query_posts(array(
              'post_type' => array( 'post', 'produtos' ),
              'cat' => $catID,
              'paged' => $paged,
              'caller_get_posts' =>1,
              'order'    => $catOrder,
              'orderby'    => $catOrderby,
            ));
            }else{
                                    
            query_posts(array(
              'post_type' => array( 'post', 'produtos' ),
              'cat' => $catID,
              'paged' => $paged,
              'caller_get_posts' =>1,
               ));
            }    
        ?>
        <?php
          $travaExibicao = false;
          $travaExibicaoOpt = get_option('wpstravaexibicao');

          global $current_user;
          get_currentuserinfo();
          $idUser = $current_user->ID;    
          $autorizacao = get_usermeta($idUser, 'wpsAutorizacao');
          $usuarioConfirmado = false;


          if( $autorizacao =='Confirmado' ){
            $usuarioConfirmado = true;
            $travaExibicao = false;
          };
 
        if( ($travaExibicaoOpt== 'sim' || $travaExibicaoOpt== 'true') && $usuarioConfirmado !=true   ){ 
          $msg  = "Você necessita de autorização para visualizar os Produtos desta loja.";
          $mensagemAutorizacao = get_option('mensagemAutorizacao2WPSHOP');
          if( is_user_logged_in() ) { 
            $mensagemAutorizacao = get_option('mensagemAutorizacaoWPSHOP');
          };

          if($mensagemAutorizacao!=""){
            $msg = $mensagemAutorizacao;
          };
          echo "<br/><br/> <h1>$msg</h1> <br/><br/>";
          }else{
        ?>

        <?php while ( have_posts() ) : the_post(); ?>

          <?php include('includes/listagem.php'); ?>

        <?php endwhile;  ?>
      <?php }; ?>      
    </div>               
    
    <?php 
      if(function_exists('wp_pagenavi')){
        wp_pagenavi();
      };  
    ?>       
  </div><!-- #conteudo -->
</div>
<?php  get_footer(); ?>