jQuery(document).ready(function(){ 
    
 
  jQuery(".cats li").hover(function(){
    jQuery(this).find('ul:first').stop().slideDown(); 
  },function(){ 
    jQuery(this).find('ul:first').stop().slideUp(); 
  });
  
  jQuery('ul.children').parent().addClass('setaMais');

  
   //jQuery(".jqzoom").jqzoom();
   
   jQuery(".imageBig").colorbox({rel:'imageBig', transition:"fade"});


    //LISTAGEM PAGINA CATEGORIAS-------------------------------------------------------------
    
     var baseUrl = jQuery('meta[name=metaurl]').attr("content");  
    
       jQuery('#filtroBuscaProduto').change(function() {      
        
        tipoFiltroV = jQuery('select#filtroBuscaProduto option:selected').val(); 
        idCatV   = jQuery('#idCat').val();   
        
        
        pagina =  baseUrl+'/includes/ajax/listagem_list.php';

          jQuery(".listagem_listCT").fadeOut();          
          jQuery(".listagem_listCT").load( pagina, {tipoFiltro:tipoFiltroV  ,idCat:idCatV }, function(){
                 jQuery(".listagem_listCT").fadeIn();         
          });
             
        
        pagina =  baseUrl+'/includes/ajax/listagem-ajax.php';
        
         jQuery(".listagemCT").fadeOut();        showBigLoad();   
         jQuery(".listagemCT").load( pagina, {tipoFiltro:tipoFiltroV  ,idCat:idCatV }, function(){
                jQuery(".listagemCT").fadeIn();        hideBigLoad();   
         });
          
    });        
    //LISTAGEM PAGINA CATEGORIA--------------------------------------------

  jQuery("[data-dropdown]").hover(function(){
    jQuery(this).toggleClass('active'); 
  });

  jQuery("[data-dropdown-mobile]").click(function(){
    jQuery(this).siblings().toggleClass('active'); 
  });
  
  jQuery(".menu-mobile .menu-item").click(function(){
    jQuery(this).toggleClass('active'); 
  });
  
  jQuery(".menu-desktop > .menu-item").hover(function(){
    jQuery(this).toggleClass('active'); 
  });


  jQuery(document).ready(function(){
    var submenu = jQuery('.menu-mobile .menu .sub-menu');
    submenu.siblings().attr('href', "javascript:void(0)")
  });

  // jQuery(function($) {
  //   jQuery(".sub-menu").parent('a').attr('href', "javascript:void(0)"); 
  // });


  jQuery(".mudarListagem ul li.col").click(function() {
    jQuery(this).addClass('ativo');
    jQuery(".mudarListagem ul li.row").removeClass('ativo');
    
    jQuery("#page .listagem_list").fadeOut();
    jQuery("#page .listagem").fadeIn();
  });
    
    
  jQuery(".mudarListagem ul li.row").click(function() {
    jQuery(this).addClass('ativo');
    jQuery(".mudarListagem ul li.col").removeClass('ativo');
    
    jQuery("#page .listagem").fadeOut();
    jQuery("#page .listagem_list").fadeIn();
  });

  jQuery('input, textarea').focus(function () {
    if (jQuery(this).val() == jQuery(this).attr("title")) {
      jQuery(this).val("");
    }
  }).blur(function () {
    if (jQuery(this).val() == "") {
      jQuery(this).val(jQuery(this).attr("title"));
    }
  });
  
   
   jQuery('.flexslider').flexslider({
    animation: "slide",
    controlNav: false
    });


     
  var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        slidesPerView: 4,
        paginationClickable: true,
        spaceBetween: 40,

        breakpoints: {
            1024: {
                slidesPerView: 4,
                spaceBetween: 15
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 30
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        },
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev'
    });


    (function () {
    var $body = document.querySelectorAll('body')[0],
        $menuAnchor = document.querySelectorAll('.menu-anchor')[0],
        prevEvent = false,
        toggleHTMLClass = function (event) {
          event.stopPropagation();
          event.preventDefault();

          if (!prevEvent) {
            prevEvent = true;

            $body.classList.toggle('menu-active');

            setTimeout(function () {
              prevEvent = false;
            }, 500);
          }
        };

    $menuAnchor.addEventListener('touchstart', toggleHTMLClass);
    $menuAnchor.addEventListener('mousedown', toggleHTMLClass);
  }(document));


  jQuery('.tabs-item').click(function(){
    var tab_id = jQuery(this).attr('data-tab');

    jQuery('.tabs-item').removeClass('current');
    jQuery('.tab-content').removeClass('current');

    jQuery(this).addClass('current');
    jQuery("#"+tab_id).addClass('current');
  });

    var nav = jQuery('.nav-top');
    
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 136) {
            nav.addClass("f-nav");
        } else {
            nav.removeClass("f-nav");
        }
    });

    // This button will increment the value
    jQuery('.qtyplus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = jQuery(this).attr('field');
        // Get its current value
        var currentVal = parseInt(jQuery('input[name='+fieldName+']').val());
        // If is not undefined
        if (!isNaN(currentVal)) {
            // Increment
            jQuery('input[name='+fieldName+']').val(currentVal + 1);
        } else {
            // Otherwise put a 0 there
            jQuery('input[name='+fieldName+']').val(0);
        }
    });
    // This button will decrement the value till 0
    jQuery(".qtyminus").click(function(e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = jQuery(this).attr('field');
        // Get its current value
        var currentVal = parseInt(jQuery('input[name='+fieldName+']').val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
            jQuery('input[name='+fieldName+']').val(currentVal - 1);
        } else {
            // Otherwise put a 0 there
            jQuery('input[name='+fieldName+']').val(0);
        }
    });

        var content = jQuery('.modal');

        //pre carregando o gif
        // loading = new Image(); loading.src = 'loading.gif';

        jQuery('.btn-watch').live('click', function( e ){
          e.preventDefault();
          content.addClass( 'active' );
          content.html( '<div class="loading4"><i></i><i></i><i></i><i></i></div>' );

          var href = jQuery( this ).attr('href');
              jQuery.ajax({
                url: href,
                success: function( response ){
                  console.log(href);
                  //forçando o parser
                  var data = jQuery( '<div>'+response+'</div>' ).find('#product-content').html();

                  //apenas atrasando a troca, para mostrarmos o loading
                  window.setTimeout( function(){
                    content.fadeOut('slow', function(){
                      content.html( data ).fadeIn();
                    });
                  }, 500 );
                }
              });

        });
   
    /*
    jQuery(".subirTopo").click(function(){
     jQuery('html, body').animate({ scrollTop: 0 }, 2000);
  }); 
  */
  
  

});