<?php include('head.php') ?>

<?php 
    $logo = custom_get_logo();     
    $logo =  esc_url( get_theme_mod( 'themeslug_logo' ));
    
    if($logo==""){
        $logo = get_bloginfo('template_url')."/images/logo.png";
    }
    $logo_mobile = get_bloginfo('template_url')."/images/marca-mobile.png";
?>
<?php
    $idPaginaPerfil = get_idPaginaPerfil(); 
    $idPaginaCarrinho = get_idPaginaCarrinho(); 
    $idPaginaCheckout = get_idPaginaCheckout();
    $idPaginaLogin = get_idPaginaLogin();
?>     

<body  <?php body_class(); ?> >
<header class="header">
    <div class="container">
        <?php $valorFreteGratis = get_option('valorFreteGratis');  ?>   
                
        <?php if($valorFreteGratis !=""){ ?>
            <p class="free"><span>Frete Grátis</span> : Para toda loja nos pedidos acima de R$<?php echo $valorFreteGratis; ?>.</p>                
         <?php }; ?>
            
        <!-- <?php include('includes/welcomeAdmin.php') ?> -->
        <div class="top">

            <div class="header-link">

                <form class="search arrend hide-phone" data-dropdown role="search"  method="get"  id="searchform" action="<?php echo home_url( '/' ); ?>" >

                <input type="text" name="s" id="searchI" value="<?php if($_REQUEST['s'] !=""){echo $_REQUEST['s']; }else{ ?>Pesquisar Produto<?php }; ?>" class="input-search" title="Pesquisar Produto" />
                <!-- <input type="submit" value="" class="input-submit" /> -->
                <button class="input-submit">
                    <i class="fa fa-search icon-header" aria-hidden="true"></i>
                </button>
            </form>
            </div>
            <a href="<?php echo verifyURL(get_bloginfo('url')); ?>" class="logo">
                <img src="<?php echo $logo; ?>" alt="<?php wp_title(); ?>" title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'   />
            </a>
                        
            <div class="header-link">
                <a class="menu-item" href="<?php echo get_permalink($idPaginaLogin); ?>"><?php echo get_the_title($idPaginaLogin); ?></a>
                <a class="menu-item" href="<?php echo get_permalink($idPaginaPerfil); ?>"><?php echo get_the_title($idPaginaPerfil); ?></a></li>   
            </div>
        </div>

        <div class="topoDir hide-phone">
            <?php if(get_post_meta(2,'telefone',true)!=""){ ?>
                <div class="telefone">
                    <span><?php echo get_post_meta(2,'telefone',true); ?></span>
                </div> 
            <?php }; ?>
        </div>
    </div> 
    
    
    <div class="menu-anchor"><i class="fa fa-bars" aria-hidden="true"></i></div>
    <div class='menu-mobile'>
        <div class="mobile-header">
            <img src="<?php echo $logo; ?>" alt="<?php wp_title(); ?>" title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'  width="120" height="40" style='margin-top: 5px'>
            <img src="<?php echo $logo_mobile ?>" alt="<?php wp_title(); ?>" width="30" height="35" class='logo-image'>
        </div>
        <?php  
            wp_nav_menu( array( 'menu' => 'Menu Topo' ,'container' => 'false','exclude' => "$idsPageExclude" ));
        ?>
    </div>

    <nav class="nav-top">
        <div class="container">
            <div class="search-fixed-menu">
            
                <form class="search arrend hide-phone"  data-dropdown role="search"  method="get"  id="searchform" action="<?php echo home_url( '/' ); ?>" >

                <input type="text" name="s" id="searchI" value="<?php if($_REQUEST['s'] !=""){echo $_REQUEST['s']; }else{ ?>Pesquisar Produto<?php }; ?>" class="input-search" title="Pesquisar Produto" />
                <!-- <input type="submit" value="" class="input-submit" /> -->
                <button class="input-submit">
                    <i class="fa fa-search icon-header" aria-hidden="true"></i>
                </button>
            </form>
            </div>
            <?php //$excludePages 
                $idsPageExclude   = get_option('idsPageExclude'); ?>
            <?php  
                wp_nav_menu( array( 'menu' => 'Menu Topo' ,'container' => 'false', 'menu_class' => 'menu-desktop','exclude' => "$idsPageExclude" ));
            ?>
            <div class="search-mobile hidden-desktop">
                <i class="fa fa-search icon-header" aria-hidden="true" data-dropdown-mobile></i>
                <form class="search arrend hide-phone search-form-mobile hidden-desktop" role="search"  method="get"  id="searchform" action="<?php echo home_url( '/' ); ?>" >
                    <input type="text" name="s" id="searchI" value="<?php if($_REQUEST['s'] !=""){echo $_REQUEST['s']; }else{ ?>Pesquisar Produto<?php }; ?>" class="input-search" title="Pesquisar Produto" />
                    <!-- <input type="submit" value="" class="input-submit" /> -->
                    <button class="input-submit">
                        <i class="fa fa-search icon-header" aria-hidden="true"></i>
                    </button>
                </form>
            </div>
            <?php include("includes/carrinhodecompra.php") ?>
        </div>
    </nav>
</header>