<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<?php require ("../../../wp-load.php");  echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>'; ?>


<?php echo '<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">'; ?>
	<?php echo '<channel>'; ?>
    
    
		<?php echo '<title>CF Care Material Hospitalar - Cadeiras de Rodas, Camas Hospitalares, etc.</title>'; ?>
		<?php echo '<link>'.get_bloginfo('url').'</link>'; ?>
		<?php echo '<description>Uma loja virtual completa para venda e aluguel de material hospitalar.</description>'; ?>
		

		<?php query_posts( 'cat=29&posts_per_page=10' ); while (have_posts()) : the_post(); ?>

		<?php
		
			  $myExcerpt = get_the_excerpt();
			  $tags = array("<p>", "</p>");
			  $myExcerpt = str_replace($tags, "", $myExcerpt);
			  
			  
			  
			$price = number_format($Product->get_product_price_only($post->ID),2);
			//$sale_price =  echo $General->get_currency_symbol() . $Product->get_product_price_sale($post->ID);
			$qtd = get_post_meta($post->ID, 'qtd', true);
			$image = get_post_meta($post->ID, 'image', true);
			
			$image_array = $General->get_post_image($post->ID);
			
			$category = get_the_category();
			
			
			$sale_price = $Product->get_product_price_sale($post->ID);
			
			

			
			
			?>

			<item>

                
                <title><?php the_title();  ?></title>
                <link><?php the_permalink();  ?></link>
                <description><?php echo $myExcerpt; ?></description>
                <g:id><?php the_ID(); ?></g:id>
                <g:condition>new</g:condition>
                <g:price><?php echo $price; ?> BRL</g:price>
                <?php if($sale_price != "0.00" AND $sale_price != "") { ?><g:sale_price><?php  echo $sale_price; ?> BRL</g:sale_price><?php }; ?>
                <g:availability>in stock</g:availability>
                

				<?php if($image_array[0]){?><g:image_link><?php echo $image_array[0]; ?></g:image_link><?php }; ?>

                
                <?php if(count($image_array)>1){?>
				<?php if($image_array[1]){?><g:additional_image_link><?php echo $image_array[1]; ?></g:additional_image_link><?php }; ?>
                <?php if($image_array[2]){?><g:additional_image_link><?php echo $image_array[2]; ?></g:additional_image_link><?php }; ?>
                <?php if($image_array[3]){?><g:additional_image_link><?php echo $image_array[3]; ?></g:additional_image_link><?php }; ?>
                <?php if($image_array[4]){?><g:additional_image_link><?php echo $image_array[4]; ?></g:additional_image_link><?php }; ?>
                <?php if($image_array[5]){?><g:additional_image_link><?php echo $image_array[5]; ?></g:additional_image_link><?php }; ?>
                <?php if($image_array[6]){?><g:additional_image_link><?php echo $image_array[6]; ?></g:additional_image_link><?php }; ?>
				<?php }; ?>
                
                
                <g:shipping>
                    <g:country>BR</g:country>
                    <g:service>Terrestre</g:service>
                    <g:price>14.95 BRL</g:price>
                </g:shipping>
                
                <!-- The following attributes are required because this item is not apparel or a custom good -->
                <g:gtin>8808992787426</g:gtin>
                <g:brand>LG</g:brand>
                <g:mpn>M2262D-PC</g:mpn>
                
                <!-- The following attributes are not required for this item, but supplying them is recommended if applicable -->
                <g:product_type><?php  echo $category[0]->cat_name; ?></g:product_type>

			</item>

<?php endwhile; ?>


<?php echo '</channel></rss>'; ?>




</body>
</html>
